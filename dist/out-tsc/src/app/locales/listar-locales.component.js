var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { MyNewServiceService } from "./../my-new-service.service";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";
var ListarLocalesComponent = /** @class */ (function () {
    function ListarLocalesComponent(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.lista = [];
        this.msgs = [];
        this.url = "http://localhost:8080";
        this.display = false;
        this.displayV = false;
        this.httpOptions = {
            headers: new HttpHeaders({
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            })
        };
    }
    ListarLocalesComponent.prototype.ngOnInit = function () {
        this.buscar();
    };
    ListarLocalesComponent.prototype.crear = function () {
        var _this = this;
        var cuerpo = {
            name: this.nombre,
            age: this.edad
        };
        this.http.post(this.url + "/patients", cuerpo, this.httpOptions).subscribe(function (data) {
            _this.messageService.add({
                severity: "success",
                summary: "Patient Created!",
                detail: ""
            });
            _this.buscar();
        }, function (error) {
            _this.messageService.add({
                severity: "error",
                summary: "Patient not Created!",
                detail: ""
            });
            console.log("error " + error);
        });
    };
    ListarLocalesComponent.prototype.buscar = function () {
        var _this = this;
        this.http.get(this.url + "/patients").subscribe(function (data) {
            _this.lista = data["content"];
        }, function (error) {
            console.log("error " + error);
        });
    };
    ListarLocalesComponent.prototype.showEdit = function (patient) {
        this.display = true;
        console.log(patient);
        this.nombre = patient.name;
        this.edad = patient.age;
        this.id = patient.id;
    };
    ListarLocalesComponent.prototype.closeEdit = function () {
        this.display = false;
    };
    ListarLocalesComponent.prototype.showVitalsDialog = function (patient) {
        this.displayV = true;
        this.nombre = patient.name;
        this.edad = patient.age;
        this.id = patient.id;
    };
    ListarLocalesComponent.prototype.closeVitalsDialog = function () {
        this.displayV = false;
    };
    ListarLocalesComponent.prototype.createVitalSigns = function () {
        var _this = this;
        var cuerpo = {
            heartRate: this.heartRate,
            systolic: this.systolicPressure,
            diastolic: this.diastolicPressure
        };
        this.http
            .post(this.url + "/patients/" + this.id + "/vitalSigns", cuerpo, this.httpOptions)
            .subscribe(function (data) {
            _this.messageService.add({
                severity: "success",
                summary: "Vital Signs Reading for " + _this.nombre + " Created!",
                detail: ""
            });
            //this.buscar();
        }, function (error) {
            _this.messageService.add({
                severity: "error",
                summary: "Vital Signs Reading not Created!",
                detail: ""
            });
            console.log("error " + error);
        });
    };
    ListarLocalesComponent.prototype.update = function (patient) {
        var _this = this;
        var cuerpo = {
            name: this.nombre,
            age: this.edad
        };
        this.http
            .put(this.url + "/patients/" + this.id, cuerpo, this.httpOptions)
            .subscribe(function (data) {
            _this.messageService.add({
                severity: "success",
                summary: "Patient Updated!",
                detail: ""
            });
            _this.buscar();
        }, function (error) {
            _this.messageService.add({
                severity: "error",
                summary: "Patient not Updated!",
                detail: ""
            });
            console.log("error " + error);
        });
        this.clear();
    };
    ListarLocalesComponent.prototype.clear = function () {
        this.nombre = null;
        this.edad = null;
        this.id = null;
    };
    ListarLocalesComponent.prototype.deletePatient = function (patient) {
        var _this = this;
        console.log(patient);
        this.http
            .delete(this.url + "/patients/" + patient.id, this.httpOptions)
            .subscribe(function (data) {
            _this.messageService.add({
                severity: "success",
                summary: "Patient Deleted!",
                detail: ""
            });
            _this.buscar();
        }, function (error) {
            _this.messageService.add({
                severity: "error",
                summary: "Patient not Deleted!",
                detail: ""
            });
            console.log("error " + error);
        });
    };
    ListarLocalesComponent = __decorate([
        Component({
            selector: "app-listar-locales",
            templateUrl: "./listar-locales.component.html",
            styleUrls: ["./listar-locales.component.css"],
            providers: [MyNewServiceService, MessageService]
        }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], ListarLocalesComponent);
    return ListarLocalesComponent;
}());
export { ListarLocalesComponent };
//# sourceMappingURL=listar-locales.component.js.map