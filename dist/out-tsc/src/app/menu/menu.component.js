var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MyNewServiceService } from './../my-new-service.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './../login/auth.service';
var MenuComponent = /** @class */ (function () {
    function MenuComponent(http, router, auth) {
        this.http = http;
        this.router = router;
        this.auth = auth;
        this.lista = [];
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    MenuComponent.prototype.buscar = function () {
        var _this = this;
        this.http.get('http://localhost:8585/local/all').subscribe(function (data) {
            _this.lista = data['localList'];
        }, function (error) {
            console.log('error ' + error);
        });
    };
    MenuComponent.prototype.logout = function () {
        this.auth.logout();
        this.router.navigate(['/login']);
    };
    MenuComponent = __decorate([
        Component({
            selector: 'app-menu',
            templateUrl: './menu.component.html',
            styleUrls: ['./menu.component.css'],
            providers: [MyNewServiceService]
        }),
        __metadata("design:paramtypes", [HttpClient,
            Router,
            AuthService])
    ], MenuComponent);
    return MenuComponent;
}());
export { MenuComponent };
//# sourceMappingURL=menu.component.js.map