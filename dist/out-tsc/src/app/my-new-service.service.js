var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Headers, Http } from "@angular/http";
import "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import { environment } from "../environments/environment";
var MyNewServiceService = /** @class */ (function () {
    function MyNewServiceService(http) {
        this.http = http;
        this.url = environment.service_uri + "/local/all";
        this.headers = new Headers({ "Content-Type": "text/plain" });
    }
    MyNewServiceService.prototype.listar = function () {
        return this.http.get(this.url, { headers: this.headers });
    };
    MyNewServiceService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], MyNewServiceService);
    return MyNewServiceService;
}());
export { MyNewServiceService };
//# sourceMappingURL=my-new-service.service.js.map