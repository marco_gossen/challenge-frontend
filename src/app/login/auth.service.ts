import { Injectable } from "@angular/core";
import { OnInit } from "@angular/core";
import { Observable } from "rxjs";
import "rxjs/add/observable/of";
import "rxjs/add/operator/do";
import "rxjs/add/operator/delay";

@Injectable()
export class AuthService implements OnInit {
  isLoggedIn: boolean = false;
  //Tabla de usuarios mock
  users = ["marco", "natura", "fede", "test"];
  // store the URL so we can redirect after logging in
  redirectUrl: string;

  ngOnInit(): void {
    this.users = ["marco", "natura", "fede", "test"];
  }

  isLogged(): boolean {
    console.log("isLogged: " + this.isLoggedIn);
    return this.isLoggedIn;
  }

  login(user: string): Observable<boolean> {
    return Observable.of(true)
      .delay(400)
      .do(val => {
        this.isLoggedIn = false;
        if (this.users.indexOf(user) !== -1) {
          this.isLoggedIn = true;
        }
      });
  }

  logout(): void {
    this.isLoggedIn = false;
  }
}
