import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DataTableModule, SharedModule } from "primeng/primeng";
import { DataTable } from "primeng/primeng";
import { Button } from "primeng/primeng";
import { MyNewServiceService } from "./../my-new-service.service";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "./../login/auth.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"],
  providers: [MyNewServiceService]
})
export class MenuComponent implements OnInit {
  results: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private auth: AuthService
  ) {}

  ngOnInit(): void {}

  logout() {
    this.auth.logout();
    this.router.navigate(["/login"]);
  }
}
