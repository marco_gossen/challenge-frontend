import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { DataTableModule, SharedModule } from "primeng/primeng";
import { DataTable } from "primeng/primeng";
import { Button } from "primeng/primeng";
import { InputTextModule } from "primeng/primeng";
import { MyNewServiceService } from "./../my-new-service.service";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";

@Component({
  selector: "app-nurse",
  templateUrl: "./nurse.component.html",
  styleUrls: ["./nurse.component.css"],
  providers: [MyNewServiceService, MessageService]
})
export class NurseComponent implements OnInit {
  lista: any;

  vitals: any;
  msgs = [];

  nombre: string;

  edad: number;

  id: string;

  idVital: string;

  systolicPressure: number;

  diastolicPressure: number;

  heartRate: number;

  url = "http://localhost:8080";

  display = false;

  displayV = false;

  displayVE = false;

  showPatients = true;

  showVitals = false;

  httpOptions = {
    headers: new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json"
    })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getPatientsData();
  }

  getPatientName(): string {
    return this.nombre;
  }

  crear() {
    if (
      this.nombre === null ||
      this.nombre === undefined ||
      this.edad === null ||
      this.edad === undefined ||
      this.edad < 0
    ) {
      this.messageService.add({
        severity: "error",
        summary: "Please complete name and age.",
        detail: ""
      });
      return;
    }
    let cuerpo: any = {
      name: this.nombre,
      age: this.edad
    };

    this.http.post(this.url + "/patients", cuerpo, this.httpOptions).subscribe(
      data => {
        this.messageService.add({
          severity: "success",
          summary: "Patient Created!",
          detail: ""
        });
        this.getPatientsData();
        this.clear();
      },
      error => {
        this.messageService.add({
          severity: "error",
          summary: "Patient not Created!",
          detail: ""
        });
        console.log("error " + error);
      }
    );
  }
  getPatientsData() {
    this.http.get(this.url + "/patients/data").subscribe(
      data => {
        this.lista = data;
      },
      error => {
        console.log("error " + error);
      }
    );
  }

  checkVitals(patient: any): String {
    let response = "No Readings";
    if (
      patient.heartRate === 0 ||
      patient.systolic === 0 ||
      patient.diastolic === 0
    ) {
      return response;
    }

    let pressure = this.checkBloodPressure(patient);
    let bpm = this.checkBpm(patient);
    response = bpm + "/" + pressure;

    return response;
  }

  checkBloodPressure(patient: any): string {
    let pressure = "Normal";
    if (patient.systolic < 120 && patient.diastolic < 80) {
      pressure = "Normal";
    } else if (
      (patient.systolic >= 120 || patient.systolic <= 129) &&
      patient.diastolic < 80
    ) {
      pressure = "Elevated";
    } else if (
      (patient.systolic >= 130 && patient.systolic <= 139) ||
      (patient.diastolic >= 80 && patient.diastolic <= 89)
    ) {
      pressure = "High-Stage 1";
    } else if (
      (patient.systolic >= 140 && patient.systolic <= 180) ||
      (patient.diastolic >= 90 && patient.diastolic <= 120)
    ) {
      pressure = "High-Stage 2";
    } else if (patient.systolic > 180 || patient.diastolic > 120) {
      pressure = "Hypertensive Crisis";
    }

    return pressure;
  }

  checkBpm(patient: any): string {
    let bpm = "Normal";
    if (
      patient.age < 1 &&
      (patient.heartRate <= 70 || patient.heartRate >= 190)
    ) {
      bpm = "Abnormal";
    } else if (
      (patient.age == 1 || patient.age == 2) &&
      (patient.heartRate <= 80 || patient.heartRate >= 160)
    ) {
      bpm = "Abnormal";
    } else if (
      (patient.age == 3 || patient.age == 4) &&
      (patient.heartRate <= 80 || patient.heartRate >= 120)
    ) {
      bpm = "Abnormal";
    } else if (
      (patient.age == 5 || patient.age == 6) &&
      (patient.heartRate <= 75 || patient.heartRate >= 115)
    ) {
      bpm = "Abnormal";
    } else if (
      (patient.age == 7 || patient.age == 9) &&
      (patient.heartRate <= 70 || patient.heartRate >= 110)
    ) {
      bpm = "Abnormal";
    } else if (
      patient.age > 9 &&
      (patient.heartRate <= 60 || patient.heartRate >= 100)
    ) {
      bpm = "Abnormal";
    }
    return bpm;
  }

  showEdit(patient: any) {
    this.display = true;
    this.nombre = patient.name;
    this.edad = patient.age;
    this.id = patient.id;
  }

  closeEdit() {
    this.display = false;
  }

  showVitalsDialog(patient: any) {
    this.displayV = true;
    this.nombre = patient.name;
    this.edad = patient.age;
    this.id = patient.id;
  }

  closeVitalsDialog() {
    this.displayV = false;
    this.clearV();
  }

  createVitalSigns() {
    // Validation 1
    if (
      this.heartRate === 0 ||
      this.diastolicPressure === 0 ||
      this.systolicPressure === 0
    ) {
      this.messageService.add({
        severity: "error",
        summary: "If any Vital Sign is 0 the Patient is not Alive..",
        detail: ""
      });
      return;
    }

    // Validation 2
    if (
      this.heartRate === null ||
      this.heartRate === undefined ||
      this.diastolicPressure === null ||
      this.diastolicPressure === undefined ||
      this.systolicPressure === null ||
      this.systolicPressure === undefined
    ) {
      this.messageService.add({
        severity: "error",
        summary: "Please complete all the Vital Signs.",
        detail: ""
      });
      return;
    }

    let cuerpo: any = {
      heartRate: this.heartRate,
      systolic: this.systolicPressure,
      diastolic: this.diastolicPressure
    };

    this.http
      .post(
        this.url + "/patients/" + this.id + "/vitalSigns",
        cuerpo,
        this.httpOptions
      )
      .subscribe(
        data => {
          this.messageService.add({
            severity: "success",
            summary: "Vital Signs Reading for " + this.nombre + " Created!",
            detail: ""
          });
          this.getPatientsData();
          this.closeVitalsDialog();
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Vital Signs Reading not Created!",
            detail: ""
          });
          console.log("error " + error);
        }
      );
  }

  update(patient: any) {
    if (
      this.nombre === null ||
      this.nombre === undefined ||
      this.edad === null ||
      this.edad === undefined ||
      this.edad < 0
    ) {
      this.messageService.add({
        severity: "error",
        summary: "Please complete name and age.",
        detail: ""
      });
      return;
    }
    let cuerpo: any = {
      name: this.nombre,
      age: this.edad
    };
    this.http
      .put(this.url + "/patients/" + this.id, cuerpo, this.httpOptions)
      .subscribe(
        data => {
          this.messageService.add({
            severity: "success",
            summary: "Patient Updated!",
            detail: ""
          });
          this.getPatientsData();
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Patient not Updated!",
            detail: ""
          });
          console.log("error " + error);
        }
      );
    this.clear();
  }

  clear() {
    this.nombre = null;
    this.edad = null;
    this.id = null;
  }

  clearV() {
    this.diastolicPressure = null;
    this.systolicPressure = null;
    this.heartRate = null;
  }

  deletePatient(patient: any) {
    this.http
      .delete(this.url + "/patients/" + patient.id, this.httpOptions)
      .subscribe(
        data => {
          this.messageService.add({
            severity: "success",
            summary: "Patient Deleted!",
            detail: ""
          });
          this.getPatientsData();
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Patient not Deleted!",
            detail: ""
          });
          console.log("error " + error);
        }
      );
  }

  patientView() {
    this.showVitals = false;
    this.showPatients = true;
    this.getPatientsData();
  }

  vitalsView(id: string) {
    this.showVitals = true;
    this.showPatients = false;
    this.id = id;
    this.getPatientVitals(id);
  }

  getPatientVitals(id: string) {
    this.http.get(this.url + "/patients/" + this.id + "/vitalSigns").subscribe(
      data => {
        console.log(data);
        this.vitals = data;
      },
      error => {
        console.log("error " + error);
      }
    );
  }

  showEditVitals(vitals: any) {
    this.displayVE = true;
    this.systolicPressure = vitals.systolic;
    this.diastolicPressure = vitals.diastolic;
    this.heartRate = vitals.heartRate;
    this.idVital = vitals.id;
  }

  closeEditVitals() {
    this.displayVE = false;
    this.systolicPressure = null;
    this.diastolicPressure = null;
    this.heartRate = null;
    this.idVital = null;
  }

  editVitalSigns() {
    // Validation 1
    if (
      this.heartRate === 0 ||
      this.diastolicPressure === 0 ||
      this.systolicPressure === 0
    ) {
      this.messageService.add({
        severity: "error",
        summary: "If any Vital Sign is 0 the Patient is not Alive..",
        detail: ""
      });
      return;
    }

    // Validation 2
    if (
      this.heartRate === null ||
      this.heartRate === undefined ||
      this.diastolicPressure === null ||
      this.diastolicPressure === undefined ||
      this.systolicPressure === null ||
      this.systolicPressure === undefined
    ) {
      this.messageService.add({
        severity: "error",
        summary: "Please complete all the Vital Signs.",
        detail: ""
      });
      return;
    }

    let cuerpo: any = {
      heartRate: this.heartRate,
      systolic: this.systolicPressure,
      diastolic: this.diastolicPressure
    };

    this.http
      .put(
        this.url + "/patients/" + this.id + "/vitalSigns/" + this.idVital,
        cuerpo,
        this.httpOptions
      )
      .subscribe(
        data => {
          this.messageService.add({
            severity: "success",
            summary: "Vital Signs Reading for " + this.nombre + " Updated!",
            detail: ""
          });
          this.getPatientVitals(this.id);
          this.closeEditVitals();
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Vital Signs Reading not Updated!",
            detail: ""
          });
          console.log("error " + error);
        }
      );
  }

  deleteVital(vital: any) {
    this.http
      .delete(
        this.url + "/patients/" + this.id + "/vitalSigns/" + vital.id,
        this.httpOptions
      )
      .subscribe(
        data => {
          this.messageService.add({
            severity: "success",
            summary: "Vital Signs Deleted!",
            detail: ""
          });
          this.getPatientVitals(this.id);
        },
        error => {
          this.messageService.add({
            severity: "error",
            summary: "Vital Signs not Deleted!",
            detail: ""
          });
          console.log("error " + error);
        }
      );
  }
}
