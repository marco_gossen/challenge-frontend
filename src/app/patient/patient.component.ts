import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/components/common/messageservice";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";

@Component({
  selector: "app-patient",
  templateUrl: "./patient.component.html",
  styleUrls: ["./patient.component.css"],
  providers: [MessageService]
})
export class PatientComponent implements OnInit {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}
  ngOnInit() {}
  name: string;

  msgs = [];

  showChart: boolean = false;
  showTable: boolean = false;

  lista: any;

  url = "http://localhost:8080";

  bpmReadings: any;

  bloodPressureChart: any;

  searchPatient() {
    this.http.get(this.url + "/patient/" + this.name).subscribe(
      data => {
        if (data && data != null) {
          this.searchPatientVitals(data["id"]);
        } else {
          this.messageService.add({
            severity: "warn",
            summary: "Patient not found.",
            detail: ""
          });
          this.clear();
        }
      },
      error => {
        this.messageService.add({
          severity: "warn",
          summary: "Patient not found.",
          detail: ""
        });
        this.clear();
        console.log("error " + error);
      }
    );
  }

  searchPatientVitals(id: string) {
    this.http.get(this.url + "/patients/" + id + "/vitalSigns").subscribe(
      data => {
        if (data && data != null) {
          this.lista = data;
          this.parseChartData(data);
          this.showChart = true;
          this.showTable = true;
        } else {
          this.messageService.add({
            severity: "warn",
            summary: "Patient doesn't have vitals recorded.",
            detail: ""
          });
        }
      },
      error => {
        this.messageService.add({
          severity: "warn",
          summary: "Patient doesn't have vitals recorded.",
          detail: ""
        });
        console.log("error " + error);
      }
    );
  }

  parseChartData(chartData: any) {
    let dates = [];
    let bpmReadings = [];
    let systolicReadings = [];
    let diastolicReadings = [];
    chartData.forEach(element => {
      dates.push(element.updatedAt);
      bpmReadings.push(element.heartRate);
      systolicReadings.push(element.systolic);
      diastolicReadings.push(element.diastolic);
    });

    this.bpmReadings = {
      labels: dates,
      datasets: [
        {
          label: "BPM Over Time",
          data: bpmReadings,
          fill: false,
          borderColor: "#4bc0c0"
        }
      ]
    };

    this.bloodPressureChart = {
      labels: dates,
      datasets: [
        {
          label: "Systolic Readings",
          data: systolicReadings,
          fill: false,
          borderColor: "#4bc0c0"
        },
        {
          label: "Diastolic Readings",
          data: diastolicReadings,
          fill: false,
          borderColor: "#565656"
        }
      ]
    };
  }

  clear() {
    this.name = null;
    this.showChart = false;
    this.showTable = false;
    this.bpmReadings = null;
    this.bloodPressureChart = null;
  }

  selectDataBpm(event) {
    this.messageService.add({
      severity: "info",
      summary: "Data Selected",
      detail: this.bpmReadings.datasets[event.element._datasetIndex].data[
        event.element._index
      ].toString()
    });
  }

  selectDataPressure(event) {
    this.messageService.add({
      severity: "info",
      summary: "Data Selected",
      detail: this.bloodPressureChart.datasets[
        event.element._datasetIndex
      ].data[event.element._index].toString()
    });
  }
}
